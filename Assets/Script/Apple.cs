﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Apple : MonoBehaviour
{
    private Vector2 temp;
    private float x = 640F, timer = 0F, timerMax = 2F, deltaAlfa = 1F;
    private RaycastHit2D rcHit;
    private bool appleHit = false;
    private Action OnDestroy;

    public void Init(Action destroy, float spawnY)
    {
        OnDestroy = destroy;
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            x = 200;
        }
        transform.position = new Vector2(x, spawnY);
    }

    private void Update() {
        if (transform.position.y > 300)
        {
            temp = transform.position;
            temp.y -= 500 * Time.deltaTime;
            transform.position = temp;
            transform.Rotate(0, 0, 20);
        }
        else
        {
            timer += Time.deltaTime;
            if (timer > timerMax)
            {
                deltaAlfa -= 0.4F * Time.deltaTime;
                gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, deltaAlfa);
            }
            if (deltaAlfa <= 0F)
            {
                if(OnDestroy != null)
                {
                    OnDestroy();
                }
            }                
        }        
    }

    public bool AppleHit()
    {
        appleHit = false;
        if (Input.GetMouseButtonDown(0))
        {
            rcHit = new RaycastHit2D(); //кидаем луч
            rcHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero); //координаты луча
            if (rcHit.collider != null)
            {
                appleHit = true;
            }
        }
        return appleHit;
    }

    public void AppleDestroy()
    {
        OnDestroy = null;
    }
}
